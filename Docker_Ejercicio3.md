## Ejercicio 3: Instalación de Wordpress

1. Crea el contenedor de la base de datos.

- Imagen: `mariadb:10.3.9`
- Nombre: `server-mysql`
- MYSQL_ROOT_PASSWORD=secret
- MYSQL_DATABASE=wordpress
- MYSQL_USER=manager
- MYSQL_PASSWORD=secret mariadb:10.3.9

Puedes seguir las instrucciones de la imagen oficial. https://hub.docker.com/_/mariadb/

Ejemplos de parametro que puedes utilizar:
- MYSQL_ROOT_PASSWORD: Variable de entorno obligatoria, indicandole la contraseña del usuario root
- MYSQL_DATABASE: Nombre de la base de datos
- MYSQL_USER: Usuario
- MYSQL_PASSWORD Contraseña del usuario

2. Crea el contenedor con wordpress. Este será creado a partir de la imagen de wordpress y lo enlazaremos con la base de datos que acabamos de crear usando el parámetro `--link`.

- Nombre: _servidor_wp_. 
- Puertos: 8080:80
- Imagen: wordpress:4.9.8
- WORDPRESS_DB_USER=manager
- WORDPRESS_DB_PASSWORD=secret
