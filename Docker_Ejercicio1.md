# Ejercicios con docker

## Ejercicio 1: Nuestros primeros contenedores

### Nuestro primer contenedor "Hola Mundo"

1. Crea un conetedor con imagen de ubuntu y un ejecute un `Hello world`.

### Ejecutando un contenedor interactivo

2. Crea un conetenedor interactivo.

Para ejecutar un contenedor interactivo usaremos él parametro `-i` para abrir la sesión interactiva y el parametro `-t` nos abritá una terminal para controlar el contenedor.
Le indicaremos un nombre al contendor y la imagen que vamos a utilizar.
Por ultimo vamos a indicarle el comando que vamos a ejecutar, en nuestro caso `/bin/bash`, esto lanzará una sesión bash.

3. Ejecuta el comando `ls -lha` con la instrucción `exec`.

### Creando un contenedor en segundo plano

4. Ejecuta un contenedor en segundo plano.

Para ejecutar un comando en el contenedor y se haga en segundo plano vamos a utilizar el parametro `-d`.

5. Borra el contenedor que acabas de crear

### Creando un contenedor con un servidor web

6. Crea un contenedor con el servicio web apache
Vamos a crear un contenedor con el servicio web apache.

- Puertos: `8080:80`
- Imagen: `httpd:2.4`

Para probar si funciona nuestra aplicación accede desde un navegador a la ip del servidor con docker y al puerto 8080.

7. Muestra los registros de logs.

Ejecutaremos `docker logs` para ver los registros.
