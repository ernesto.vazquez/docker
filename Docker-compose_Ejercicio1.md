# Ejercicio 1: Nuestro primer compose
## Network
1. Definir un compose con la red llamada mynetwork y definir el usaremos el rango: `172.19.1.0/24`

## Volumen persistente
2. Definir un volumen persistente tanto para una base de datos, debe guardarse en /opt/db, para un servidor web /opt/web

## Primer compose: Base de datos y wordpress
3. Crear un compose con un contenedor como base de datos `mysql`
4. Agrega al compose un servicio wordpress, https://docs.docker.com/samples/wordpress/

# Asignar IPs
5. La base de datos tendrá la ip: `172.19.1.2`
6. El servidor web tendrá la ip: `172.19.1.3`

# Puertos en los servicios
7. El servidor web expondrá el puerto `8000`

# Objetivos
8. Arrancar el compose, abrir el wordpress y crear un usuario
9. Hacer un down en el compose y volver a hacer un up, comprobando que los usuarios se persisten.


