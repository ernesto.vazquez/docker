## Ejercicio 2: Creando nuestras imágenes con Dockerfile

Vamos a crear una página estática con apache 2.4, primero vamos a crear un nuevo directorio donde vamos a guardar toda nueva página y vamos a crear un archivo html con una texto de prueba.

`$ echo "<h1>Prueba</h1>" > index.html`

1. Crear el archivo `Dockerfile`, con las siguientes instrucciones:

- Imagen: `debian`
- Commandos con run: `apt-get update -y && apt-get install -y apache2 && apt-get clean && rm -rf /var/lib/apt/lists/*`
- Copiar el archivo `index.html` al directorio `/var/www/html/`
- ENTRYPOINT: `["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]`

2. Crea la imagen que has definido en el fichero `Dockerfile`:

* Comprueba que hemos creado una nueva imagen, con `docker images`

3. Finalmente, crea el contenedor a partir de la nueva imagen.

* Recuerda agregar un nombre al conetenedor y los puertos 80:80.

4. Sube la imagen a DockerHub
