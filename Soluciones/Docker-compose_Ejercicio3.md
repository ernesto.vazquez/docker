# Reserva de recursos y límites
## Asignar reserva de recursos  al compose anterior

- **Memoria limite:** 100M
- **Memoria reservada:** 50M
- **CPUs:** 0.2

**Limites y recursos en docker-compose:**

```
mem_limit: 100M
mem_reservation: 50M
cpus: 0.2
```

**Archivo compose completo:**

```
version: '3.5'
networks:
  mynetwork:
      ipam:
        driver: default
        config:
          - subnet: 172.29.1.0/24
services:
  db:
    image: mysql:5.7
    volumes:
      - /opt/db:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.2
    mem_limit: 100M
    mem_reservation: 50M
    cpus: 0.2

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - /opt/web:/var/www/html
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.3
    mem_limit: 100M
    mem_reservation: 50M
    cpus: 0.2
```
