## Ejercicio 2:
Al ejercicio anterior le añadiremos phpmyadmin y un nginx como proxy para que al entrar en el dominio localhost nos dirija a la web y al entrar en el location /phpmyadmin nos redirija a administrador de la base de datos.


https://hub.docker.com/r/phpmyadmin/phpmyadmin/

 

Vamos a agregar el servicio de phpmyadmin

```
  phpmyadmin:
    image: phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      - PMA_ARBITRARY=1
```

Docker-compose completo:

```
version: '3.5'
networks:
  mynetwork:
      ipam:
        driver: default
        config:
          - subnet: 172.29.1.0/24
services:
  db:
    image: mysql:5.7
    volumes:
      - /opt/db:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.2
    mem_limit: 1024m
    mem_reservation: 256M
    cpus: 0.2

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - /opt/web:/var/www/html
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.3
    mem_limit: 1024m
    mem_reservation: 256M
    cpus: 0.2

  phpmyadmin:
    image: phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      - PMA_ARBITRARY=1
```

Accedemos a traves del puerto 8080:

![](https://i.imgur.com/Vm2ENLi.png)

Para entrar en el location **/phpmyadmin** nos redirija a administrador de la base de datos. Podremos hacerlo mediante un **Reverse Proxy.**

- **Ejemplo:** https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/

```
server {
        listen 80 default_server;
        location /phpmyadmin {
            proxy_pass http://127.0.0.1:8080;
            proxy_pass_request_headers on;
            proxy_set_header Host $host;
        }
}
```

