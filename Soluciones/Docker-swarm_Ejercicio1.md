# Cluster Docker Swarm
## Crear un cluster con docker swarm.

### Requisitos 
- A Crear un cluster con Docker Swarm al menos con un nodo managuer y otro worker.
- B Crear un stack con wordpress, mysql, phpmyadmin, traefik y portainer.

### A Crear un cluster con Docker Swarm al menos con un nodo managuer y otro worker.

#### En el nodo managuer ejecutaremos: 

Comando:
    docker swarm init

Resultado: 
"Las instrucciones para agregar nodos managuer o worker"

Swarm initialized: current node (068owutx5w3qbznk52dkbwi3v) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-47m3jazc9gu1crbgv3izyedz8tz2zx6pwt2x7stxxx1d9ahozd-ahx0x43je2dogcb4nw5x81o8i 192.168.65.3:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.


#### En el nodo worker ejecutaremos algo similar a: 
"Fijarse que las direcciones IP cambian según la máquina virtual"
docker swarm join --token SWMTKN-1-47m3jazc9gu1crbgv3izyedz8tz2zx6pwt2x7stxxx1d9ahozd-ahx0x43je2dogcb4nw5x81o8i 192.168.65.3:2377


#### En el nodo managuer comprobamos el estado del cluster:
Comando: 
    docker nodes ls


    ID                            HOSTNAME         STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
    068owutx5w3qbznk52dkbwi3v *   docker-desktop   Ready     Active         Leader           20.10.8   

### B Crear Stack wordpress, mysql, phpmyadmin, traefik y portainer

    version: "3"
    services:
      traefik:
        image: "traefik:v2.6"
        container_name: "traefik"
    command:
          #- "--log.level=DEBUG"
          - "--api.insecure=true"
          - "--providers.docker=true"
          - "--providers.docker.exposedbydefault=false"
          - "--entrypoints.web.address=:80"
        ports:
          - "80:80"
          - "8080:8080"
        networks:
          - mynetwork
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock:ro"
      db:
        image: mysql:5.7
        networks:
          - mynetwork
        volumes:
          - myvolume:/var/lib/mysql
        environment:
          MYSQL_ROOT_PASSWORD: mywordpress
          MYSQL_DATABASE: wordpress
        deploy:
          placement:
            constraints:
              - node.role == manager
      phpmyadmin:
        image: phpmyadmin/phpmyadmin
        labels:
          - "traefik.enable=true"
          - "traefik.http.routers.phpmyadmin.rule=Host(`phpmyadmin.localhost`)"
          - "traefik.http.routers.phpmyadmin.entrypoints=web"
        environment:
          - PMA_HOST=db
          - PMA_USER=root
          - PMA_PASSWORD=mywordpress
        networks:
          - mynetwork              
      wordpress:
        image: wordpress
        labels:
          - "traefik.enable=true"
          - "traefik.http.routers.wordpress.rule=Host(`wordpress.localhost`)"
          - "traefik.http.routers.wordpress.entrypoints=web"
          - "traefik.backend.loadbalancer.sticky=true"
        networks:
          - mynetwork
        environment:
          WORDPRESS_DB_PASSWORD: mywordpress
          WORDPRESS_DB_HOST: db:3306
          WORDPRESS_DB_USER: root
        deploy:
          placement:
            constraints:
              - node.role == manager
      agent:
        image: portainer/agent:2.11.0
        volumes:
          - /var/run/docker.sock:/var/run/docker.sock
          - /var/lib/docker/volumes:/var/lib/docker/volumes
        networks:
          - mynetwork
        deploy:
          mode: global
          placement:
            constraints: [node.platform.os == linux]

      portainer:
        image: portainer/portainer-ce:2.11.0
        command: -H tcp://tasks.agent:9001 --tlsskipverify
        ports:
          - "9443:9443"
          - "9000:9000"
          - "8000:8000"
        volumes:
          - portainer_data:/data
        networks:
          - mynetwork
        deploy:
          mode: replicated
          replicas: 1
          placement:
            constraints: [node.role == manager]

    volumes:
      portainer_data:
      myvolume:
    networks:
      mynetwork:
