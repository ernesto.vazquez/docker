## Ejercicio 2: Creando nuestras imágenes con Dockerfile

Vamos a crear una página estática con apache 2.4, primero vamos a crear un nuevo directorio donde vamos a guardar toda nueva página y vamos a crear un archivo html con una texto de prueba.

```
$ echo "<h1>Prueba</h1>" > index.html
```

Creamos el archivo `Dockerfile`:

```
FROM debian
RUN apt-get update -y && apt-get install -y \
        apache2 \
        && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY . /var/www/html/
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
```

Creamos la imagen que hemos definido en el fichero `Dockerfile`:

```
docker build -t pruebaweb/aplicacionweb:v1 .
```

* Comprueba que hemos creado una nueva imagen

```
$ docker images 
REPOSITORY                                                  TAG                  IMAGE ID       CREATED         SIZE
pruebaweb/aplicacionweb                                     v1                   239f9ba3926c   2 minutes ago   235MB
```

Finalmente creamos un contenedor a partir de nuestra nueva imagen:

`$ docker run --name aplicacionweb -d -p 80:80 pruebaweb/aplicacionweb:v1`

Resultado via web:

![](https://i.imgur.com/Ux09VXk.png)

Para subir la imagen a Docker Hub, primero tendremos que tener una cuenta creada y un repositorio.

Primero le ponemos un tag a la imagen:

`$ docker image tag debian:latest subirimagendebian:1.0`

Tendremos que iniciar sesión con nuestra cuenta de docker

```
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: *******
Password: 
WARNING! Your password will be stored unencrypted in /home/devops/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Por último subimos la imagen a nuestro repositorio de Docker Hub.

`$ docker push usuario/repositorio:1.0`

