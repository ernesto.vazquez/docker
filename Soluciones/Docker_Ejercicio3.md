## Ejercicio 3: Instalación de Wordpress

Lo primero que vamos a hacer es crear un contenedor desde la imagen mariadb con el nombre `server-mysql`.

Puedes seguir las instrucciones de la imagen oficial. https://hub.docker.com/_/mariadb/

```
docker run -d --name wordpress-db \
    -e MYSQL_ROOT_PASSWORD=secret \
    -e MYSQL_DATABASE=wordpress \
    -e MYSQL_USER=manager \
    -e MYSQL_PASSWORD=secret mariadb:10.3.9
```


Vamos a ver que hace cada parametro:
- MYSQL_ROOT_PASSWORD: Variable de entorno obligatoria, indicandole la contraseña del usuario root
- MYSQL_DATABASE: Nombre de la base de datos
- MYSQL_USER: Usuario
- MYSQL_PASSWORD Contraseña del usuario

A continuación vamos a crear un nuevo contenedor, con el nombre `servidor_wp`. Este será creado a partir de la imagen de wordpress y lo enlazaremos con la base de datos que acabamos de crear usando el parámetro `--link`.

```
docker run -d --name wordpress \
    --link wordpress-db:mysql \
    -e WORDPRESS_DB_USER=manager \
    -e WORDPRESS_DB_PASSWORD=secret \
    -p 8080:80 \
    wordpress:4.9.8
```


![](https://i.imgur.com/rBqUjus.png)

![](https://i.imgur.com/W7hP3LQ.png)
