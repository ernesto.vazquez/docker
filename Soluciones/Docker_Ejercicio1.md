# Ejercicios con docker

## Ejercicio 1: Nuestros primeros contenedores

### Nuestro primer contenedor "Hola Mundo"

```
$ docker run ubuntu /bin/echo 'Hello world'
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
08c01a0ec47e: Pull complete 
Digest: sha256:669e010b58baf5beb2836b253c1fd5768333f0d1dbcb834f7c07a4dc93f474be
Status: Downloaded newer image for ubuntu:latest
Hello world
```

El comando `run` va a ejecutar un contenedor con un comando a partir de la imagen de ubuntu. 
Si todavia no tenemos dicha imagen en nuestra maquina, lo primero que hará será descargar la imagen. 


### Ejecutando un contenedor interactivo

Para ejecutar un contenedor interactivo usaremos él parametro `-i` para abrir la sesión interactiva y el parametro `-t` nos abritá una terminal para controlar el contenedor.

Le indicaremos un nombre al contendor y la imagen que vamos a utilizar.

Por ultimo vamos a indicarle el comando que vamos a ejecutar, en nuestro caso `/bin/bash`, esto lanzará una sesión bash.

```
$ docker run -it --name contenedor1 ubuntu /bin/bash 
root@2053a8eeb4f7:/# 
```

El contenedor se para cuando salimos de él. Para volver a conectarnos a él:

```
$ docker start contenedor1 
contenedor1

$ docker ps
CONTAINER ID   IMAGE     COMMAND       CREATED              STATUS         PORTS     NAMES
2053a8eeb4f7   ubuntu    "/bin/bash"   About a minute ago   Up 3 seconds             contenedor1


$ docker attach contenedor1 
root@2053a8eeb4f7:/# 
```

Podemos ejecutar comando en el contenedor con el subcomando `exec`:

```
$ docker start contenedor1
$ docker exec contenedor1 ls -al
```

### Creando un contenedor demonio

Para ejecutar un comando en el contenedor y se haga en segundo plano vamos a utilizar el parametro `-d`.

```
$ docker run -d --name contenedor2 ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done"
3fa277c468902bda26b1fe8ddd6c6905586b5eaf345b8e2463445321cb48b17f

$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS     NAMES
3fa277c46890   ubuntu    "/bin/sh -c 'while t…"   3 seconds ago   Up 2 seconds             contenedor2
2053a8eeb4f7   ubuntu    "/bin/bash"              8 minutes ago   Up 4 minutes             contenedor1

$ docker logs contenedor2
hello world
hello world
hello world
hello world
```

Por último podemos parar el contenedor y borrarlo con las siguientes instrucciones:

```
$ docker stop contenedor2
contenedor2
$ docker rm contenedor2
contenedor2
```

### Creando un contenedor con un servidor web

Vamos a crear un contenedor con el servicio web apache

```
$ docker run -d --name contenedor-apache -p 8080:80 httpd:2.4
```

Vemos que el contenedor se está ejecutando, además con la opción `-p` mapeamos un puerto del equipo donde tenemos instalado el docker, con un puerto del contenedor.  Para probarlo accede desde un navegador a la ip del servidor con docker y al puerto 8080.

![](https://i.imgur.com/Z1kaYPj.png)

Ejecutaremos `docker logs` para ver los registros.

```
$ docker logs contenedor-apache 
```
