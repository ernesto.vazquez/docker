## Definir un compose con la red llamada mynetwork y definir el usaremos el rango 172.19.1.0/24

```
version: '3.5'
networks:
  mynetwork:
      ipam:
        driver: default
        config:
          - subnet: 172.29.1.0/24
```

## Definir un volumen persistente tanto para una base de datos, debe guardarse  en /opt/db, para un servidor web /opt/web

- Volumen base de datos:
```
    volumes:
      - /opt/db:/var/lib/mysql
```

- Volumen wordpress:
```
    volumes:
      - /opt/web:/var/www/html
```

```
$ ls -lha /opt/db
total 185M
drwxr-xr-x  6 systemd-coredump root             4,0K feb  7 09:48 .
drwxr-xr-x 13 root             root             4,0K feb  7 09:39 ..
-rw-r-----  1 systemd-coredump systemd-coredump   56 feb  7 09:41 auto.cnf
-rw-------  1 systemd-coredump systemd-coredump 1,7K feb  7 09:41 ca-key.pem
-rw-r--r--  1 systemd-coredump systemd-coredump 1,1K feb  7 09:41 ca.pem
-rw-r--r--  1 systemd-coredump systemd-coredump 1,1K feb  7 09:41 client-cert.pem
-rw-------  1 systemd-coredump systemd-coredump 1,7K feb  7 09:41 client-key.pem
-rw-r-----  1 systemd-coredump systemd-coredump  886 feb  7 09:48 ib_buffer_pool
-rw-r-----  1 systemd-coredump systemd-coredump  76M feb  7 09:48 ibdata1
-rw-r-----  1 systemd-coredump systemd-coredump  48M feb  7 09:48 ib_logfile0
-rw-r-----  1 systemd-coredump systemd-coredump  48M feb  7 09:41 ib_logfile1
-rw-r-----  1 systemd-coredump systemd-coredump  12M feb  7 09:48 ibtmp1
drwxr-x---  2 systemd-coredump systemd-coredump 4,0K feb  7 09:41 mysql
drwxr-x---  2 systemd-coredump systemd-coredump 4,0K feb  7 09:41 performance_schema
-rw-------  1 systemd-coredump systemd-coredump 1,7K feb  7 09:41 private_key.pem
-rw-r--r--  1 systemd-coredump systemd-coredump  452 feb  7 09:41 public_key.pem
-rw-r--r--  1 systemd-coredump systemd-coredump 1,1K feb  7 09:41 server-cert.pem
-rw-------  1 systemd-coredump systemd-coredump 1,7K feb  7 09:41 server-key.pem
drwxr-x---  2 systemd-coredump systemd-coredump  12K feb  7 09:41 sys
drwxr-x---  2 systemd-coredump systemd-coredump 4,0K feb  7 09:48 wordpress

$ ls -lha /opt/web/
total 244K
drwxr-xr-x  5 www-data www-data 4,0K feb  7 09:48 .
drwxr-xr-x 13 root     root     4,0K feb  7 09:39 ..
-rw-r--r--  1 www-data www-data  553 feb  7 09:48 .htaccess
-rw-r--r--  1 www-data www-data  405 feb  6  2020 index.php
-rw-r--r--  1 www-data www-data  20K ene  1 01:15 license.txt
-rw-r--r--  1 www-data www-data 7,3K dic 28 18:38 readme.html
-rw-r--r--  1 www-data www-data 7,0K ene 21  2021 wp-activate.php
drwxr-xr-x  9 www-data www-data 4,0K feb  7 09:47 wp-admin
-rw-r--r--  1 www-data www-data  351 feb  6  2020 wp-blog-header.php
-rw-r--r--  1 www-data www-data 2,3K nov 10 00:07 wp-comments-post.php
-rw-rw-r--  1 www-data www-data 5,4K ene 29 00:31 wp-config-docker.php
-rw-r--r--  1 www-data www-data 5,5K feb  7 09:41 wp-config.php
-rw-r--r--  1 www-data www-data 3,0K dic 14 09:44 wp-config-sample.php
drwxr-xr-x  7 www-data www-data 4,0K feb  7 09:48 wp-content
-rw-r--r--  1 www-data www-data 3,9K ago  3  2021 wp-cron.php
drwxr-xr-x 26 www-data www-data  16K ene 25 20:50 wp-includes
-rw-r--r--  1 www-data www-data 2,5K feb  6  2020 wp-links-opml.php
-rw-r--r--  1 www-data www-data 3,9K may 15  2021 wp-load.php
-rw-r--r--  1 www-data www-data  47K ene  4 09:30 wp-login.php
-rw-r--r--  1 www-data www-data 8,4K sep 22 23:01 wp-mail.php
-rw-r--r--  1 www-data www-data  23K nov 30 18:32 wp-settings.php
-rw-r--r--  1 www-data www-data  32K oct 25 02:23 wp-signup.php
-rw-r--r--  1 www-data www-data 4,7K oct  8  2020 wp-trackback.php
-rw-r--r--  1 www-data www-data 3,2K jun  8  2020 xmlrpc.php
```

## Crear un compose con un contenedor como base de datos  “mysql”.

```
services:
  db:
    image: mysql:5.7
    volumes:
      - /opt/db:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
```


## El objetivo es montar un wordpress https://docs.docker.com/samples/wordpress/

```
services:  
  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - /opt/web:/var/www/html
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
```

## La base de datos tendrá la ip 172.19.1.2

```
services:
  db:
    networks:
      mynetwork:
        ipv4_address: 172.29.1.2
```

## El servidor web tendrá la ip 172.19.1.3

```
services:
  wordpress:
    networks:
      mynetwork:
        ipv4_address: 172.29.1.3
```

## El servidor web expondrá el puerto 8000

```
services:
  wordpress:
    ports:
      - "8000:80"
```

## Objetivo 1: Arrancar el compose, abrir el wordpress y crear un usuario

`docker-compose up -d`


![](https://i.imgur.com/CIzeL4r.png)

![](https://i.imgur.com/WDWTExr.png)

![](https://i.imgur.com/PAFi5OV.png)


## Objetivo 2: Hacer un down en el compose y volver a hacer un up, comprobando que los usuarios se persisten.

`docker-compose down`

Despues de crear un usuario de prueba, apagar el compose y posteriormente levantarlo, podemos ver que se mantienen los datos persistidos.

![](https://i.imgur.com/k44JAly.png)

Si necesitamos borrar los datos persistidos solamente tendremos que eliminar los contenidos de los directorios /opt/db y /opt/web

## Docker-compose completo:

```
version: '3.5'
networks:
  mynetwork:
      ipam:
        driver: default
        config:
          - subnet: 172.29.1.0/24
services:
  db:
    image: mysql:5.7
    volumes:
      - /opt/db:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.2
    
  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - /opt/web:/var/www/html
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    networks:
      mynetwork:
        ipv4_address: 172.29.1.3
```

```
$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                                   NAMES
e33345032b50   wordpress:latest   "docker-entrypoint.s…"   2 seconds ago   Up 2 seconds   0.0.0.0:8000->80/tcp, :::8000->80/tcp   wordpress_1
c53765bc72a7   mysql:5.7          "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   3306/tcp, 33060/tcp                     db_1
```
