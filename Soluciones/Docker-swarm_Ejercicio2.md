# Cluster Docker Swarm 
##  Secrets, Volumenes y replicas en un cluster con docker swarm.

### Requisitos 
- Las contraseñas deberan ser creadas mediantes secrets

printf root | docker secret create user -
printf mywordpress | docker secret create passworddb -


    docker secret ls
    ID                          NAME         DRIVER    CREATED       UPDATED
    6lrxrokitcjq07krx84hkfgt3   passworddb             2 hours ago   2 hours ago
    vk53c0ccg69sjm40i0ic8u7fq   user                   2 hours ago   2 hours ago



- Persistir los datos de mysql en el nodo managuer y configurar para que siempre arranque en el nodo managuer.

    mkdir dbdata 
    mkdir portainer_data

    
